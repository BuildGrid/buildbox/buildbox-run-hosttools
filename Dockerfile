FROM registry.gitlab.com/buildgrid/buildbox/buildbox-worker:latest

# Install coreutils so there are some CLIs to play with
RUN apt-get update -y
RUN apt-get install coreutils -y

COPY . /buildbox-run-hosttools

RUN cd /buildbox-run-hosttools && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=DEBUG .. && \
    make && \
    CTEST_OUTPUT_ON_FAILURE=1 make test

ENV PATH "/buildbox-run-hosttools/build:$PATH"

# Build Args to set default Server and CAS Server
ARG SERVER="http://127.0.0.1:50051"
ARG CAS_SERVER=${SERVER}
ARG INSTANCE="dev"

# Add as ENV (to use during runtime)
ENV BUILDGRID_SERVER_URL=${SERVER}
ENV CAS_SERVER_URL=${CAS_SERVER}
ENV BUILDGRID_INSTANCE=${INSTANCE}

# Default entry point
CMD buildbox-worker --verbose --buildbox-run=buildbox-run-hosttools --bots-remote=${BUILDGRID_SERVER_URL} --cas-remote=${CAS_SERVER_URL} --instance=${BUILDGRID_INSTANCE}
