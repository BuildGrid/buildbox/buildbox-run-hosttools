Transition notice
=================

This repository does not accept MRs anymore, as it is being merged into `buildbox`
per https://gitlab.com/BuildGrid/buildbox/buildbox-common/-/issues/92

What is buildbox-run-hosttools?
===============================

``buildbox-run-hosttools`` attempts to run actions without any sandboxing.

Usage
=====
::

    usage: ./buildbox-run-hosttools [OPTIONS]
        --action=PATH               Path to read input Action from
        --action-result=PATH        Path to write output ActionResult to
        --log-level=LEVEL           (default: info) Log verbosity: trace/debug/info/warning/error
        --verbose                   Set log level to debug
        --log-directory=DIR         Write logs to this directory with filenames:
                                    buildbox-run-hosttools.<hostname>.<user name>.log.<severity level>.<date>.<time>.<pid>
        --disable-localcas          Do not use LocalCAS protocol methods
        --workspace-path=PATH       Location on disk which runner will use as root when executing jobs
        --stdout-file=FILE          File to redirect the command's stdout to
        --stderr-file=FILE          File to redirect the command's stderr to
        --no-logs-capture           Do not capture and upload the contents written to stdout and stderr
        --collect-execution-stats   Gather `execution_stats.proto` metrics and attach the digest of the message
                                    in CAS to `ActionResult.execution_metadata.auxiliary_metadata`
        --validate-parameters       Only check whether all the required parameters are being passed and that no
                                    unknown options are given. Exits with a status code containing the result (0 if successful).
        --remote=URL                URL for CAS service
        --instance=NAME             Name of the CAS instance
        --server-cert=PATH          Public server certificate for TLS (PEM-encoded)
        --client-key=PATH           Private client key for TLS (PEM-encoded)
        --client-cert=PATH          Public client certificate for TLS (PEM-encoded)
        --access-token=PATH         Access Token for authentication (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorizat
    ion bearer token.
        --token-reload-interval=MINUTES Time to wait before refreshing access token from disk again. The following suffixes can be optionally s
    pecified: M (minutes), H (hours). Value defaults to minutes if suffix not specified.
        --googleapi-auth            Use GoogleAPIAuth when this flag is set.
        --retry-limit=INT           Number of times to retry on grpc errors
        --retry-delay=MILLISECONDS  How long to wait before the first grpc retry
        --request-timeout=SECONDS   How long to wait for gRPC request responses
        --load-balancing-policy     Which grpc load balancing policy to use. Valid options are 'round_robin' and 'grpclb'
        --ra-remote=URL             URL for Asset service
        --ra-instance=NAME          Name of the Asset instance
        --ra-server-cert=PATH       Public server certificate for TLS (PEM-encoded)
        --ra-client-key=PATH        Private client key for TLS (PEM-encoded)
        --ra-client-cert=PATH       Public client certificate for TLS (PEM-encoded)
        --ra-access-token=PATH      Access Token for authentication (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorizat
    ion bearer token.
        --ra-token-reload-interval=MINUTES Time to wait before refreshing access token from disk again. The following suffixes can be optionall
    y specified: M (minutes), H (hours). Value defaults to minutes if suffix not specified.
        --ra-googleapi-auth         Use GoogleAPIAuth when this flag is set.
        --ra-retry-limit=INT        Number of times to retry on grpc errors
        --ra-retry-delay=MILLISECONDS How long to wait before the first grpc retry
        --ra-request-timeout=SECONDS How long to wait for gRPC request responses
        --ra-load-balancing-policy  Which grpc load balancing policy to use. Valid options are 'round_robin' and 'grpclb'
        --allow-executable=ALLOWED_EXECUTABLE_PATH Only commands which run this executable are allowed.
